
import collections
import contextlib
import sys
import wave

import webrtcvad
import subprocess
import time



def frame_generator(frame_duration_ms, audio, sample_rate, start_chunk):
    """Generates audio frames from PCM audio data.

    Takes the desired frame duration in milliseconds, the PCM data, and
    the sample rate.

    Yields Frames of the requested duration.
    """
    n = int(sample_rate * (frame_duration_ms / 1000.0) * 2)
    offset = 0
    timestamp = start_chunk
    duration = (float(n) / sample_rate) / 2.0

    while offset + n <= len(audio):
        yield Frame(audio[offset:offset + n], timestamp, duration)
        timestamp += n

        offset += n


class Frame(object):
    """Represents a "frame" of audio data."""
    def __init__(self, bytes, timestamp, duration):
        self.bytes = bytes
        self.timestamp = timestamp
        self.duration = duration

class Audio_segment():
    def __init__(self, id, temp_dir, sample_rate, start_time):
        self.segment_id = id
        self.sample_rate = sample_rate
        self.adc_file = "%s/recording.adc.en-EN.%s" % (temp_dir, str(id))
        self.start_time = start_time
        self.file = wave.open(self.adc_file, 'wb')
        self.tempdir = temp_dir
        self.file.setnchannels(1)
        self.file.setsampwidth(2)
        self.file.setframerate(sample_rate)

    def make_ready_file(self):
        subprocess.call(["touch %s-ready" % self.adc_file], shell=True)

    def make_start_file(self):
        subprocess.call(["touch %s-start" % self.adc_file], shell=True)

    def make_idx_file(self):
        with open("%s/recording.adc.en-EN.idx" % self.tempdir, "w") as f:
            f.write(str(self.segment_id))
        return 0

    def make_info_file(self):
        with open("%s-info" % self.adc_file, "w") as f:
            f.write("{startS %s}" % str(self.start_time))

    def write_fgets_file(self):
        with open("%s/recording.adc.en-EN.fgets" % self.tempdir, "a") as f:
            f.write(self.adc_file + "\n")
        return 0

    def add(self, frame):
        self.file.writeframes(frame.bytes)

    def save(self):
        self.file.close()

        return 0


class Segmentor():
    def __init__(self, temp_dir, sample_rate, VAD_aggressive, padding_duration_ms, frame_duration_ms, rate_begin, rate_end):

        self.idx = 0
        self.temp_dir = temp_dir
        self.vad = webrtcvad.Vad(VAD_aggressive)
        self.padding_duration_ms = padding_duration_ms
        self.sample_rate = sample_rate
        self.frame_duration_ms = frame_duration_ms
        num_padding_frames = int(self.padding_duration_ms / self.frame_duration_ms)
        self.ring_buffer = collections.deque(maxlen=num_padding_frames)
        self.triggered = False
        self.length_of_frame = int(self.sample_rate * (self.frame_duration_ms / 1000.0) * 2)
        self.temp = b''
        self.rate_begin = rate_begin
        self.rate_end = rate_end
        self.start_chunk = 0
        self.segment = None


    def reset(self):
        self.triggered = False
        self.length_of_frame = int(self.sample_rate * (self.frame_duration_ms / 1000.0) * 2)
        self.temp = b''
        self.start_chunk = 0
        self.segment = None
        self.ring_buffer.clear()
        self.idx = 0
        subprocess.call(["touch %s/recording.adc.en-EN.fgets" % self.temp_dir], shell=True)


    def add_audio(self, audio):
        audio = self.temp + audio
        length_audio = len(audio) // self.length_of_frame * self.length_of_frame
        self.temp = audio[length_audio:]
        audio = audio[:length_audio]
        frames = frame_generator(30, audio, self.sample_rate, self.start_chunk)
        frames = list(frames)
        self.start_chunk = self.start_chunk + len(audio)
        for j, frame in enumerate(frames):
            is_speech = self.vad.is_speech(frame.bytes, self.sample_rate)
            if not self.triggered:
                self.ring_buffer.append((frame, is_speech))
                num_voiced = len([f for f, speech in self.ring_buffer if speech])

                if num_voiced >  self.rate_begin * self.ring_buffer.maxlen:
                    self.triggered = True
                    start_time = self.ring_buffer[0][0].timestamp // 2

                    self.segment = Audio_segment(self.idx, self.temp_dir, self.sample_rate, start_time)
                    for f, s in self.ring_buffer:
                        self.segment.add(f)

                    self.segment.make_info_file()
                    self.segment.make_start_file()
                    self.segment.make_idx_file()

                    self.segment.write_fgets_file()

                    self.ring_buffer.clear()
            else:
                self.segment.add(frame)
                self.ring_buffer.append((frame, is_speech))
                num_unvoiced = len([f for f, speech in self.ring_buffer if not speech])
                if num_unvoiced >  self.rate_end * self.ring_buffer.maxlen:
                    self.triggered = False
                    self.idx += 1
                    # ready adc
                    # end info adc + end time
                    self.segment.save()
                    self.segment.make_ready_file()
                    self.ring_buffer.clear()













